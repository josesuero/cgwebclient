'use strict';

/**
 * @ngdoc overview
 * @name cgWebclientApp
 * @description # cgWebclientApp
 * 
 * Main module of the application.
 */

var realm = '';
var auth = {};
var logout = function() {
	console.log('*** LOGOUT');
	auth.loggedIn = false;
	auth.authz = null;
	window.location = auth.logoutUrl;
};

angular.element(document).ready(
		function($http) {
			var location = window.location;
			var host = /^(.*?)\..*/.exec(location.hostname);
			if (host.length > 1) {
				realm = host[1];
			}
			var keycloakAuth = new Keycloak({
				url : window.location.origin + '/auth',
				realm : realm,
				clientId : 'itineris'
			});
			auth.loggedIn = false;

			keycloakAuth.init({
				onLoad : 'login-required'
			}).success(
					function(token) {
						auth.loggedIn = true;
						auth.authz = keycloakAuth;
						auth.logoutUrl = keycloakAuth.authServerUrl
								+ "/realms/" + realm
								+ "/tokens/logout?redirect_uri="
								+ encodeURIComponent(window.location.href);
						angular.bootstrap(document, [ "cgWebclientApp" ]);
					}).error(function(e) {
				alert(e);
			});
		});

angular
		.module(
				'cgWebclientApp',
				[ 'ngAnimate', 'ngCookies', 'ngResource', 'ngRoute',
						'ngSanitize', 'ngTouch', 'pascalprecht.translate',
						'tmh.dynamicLocale' ])
		.constant('LOCALES', {
			'locales' : {
				'es' : 'Español',
				'en' : 'English'
			},
			'preferredLocale' : 'en'
		})

		.config(function($translateProvider) {
			$translateProvider.useMissingTranslationHandlerLog();
			$translateProvider.useStaticFilesLoader({
				prefix : '/cgWebclient/clearg/server/json/cgapp.cgap_labels_single.cg?data=' + encodeURIComponent('{"lang":"'),
				suffix : encodeURIComponent('"}')
			});
			$translateProvider.preferredLanguage('en');
			$translateProvider.useLocalStorage();
			$translateProvider.useLocalStorage();
			$translateProvider.useSanitizeValueStrategy('sanitize');
		})
		.config(
				function(tmhDynamicLocaleProvider) {
					tmhDynamicLocaleProvider
							.localeLocationPattern('bower_components/angular-i18n/angular-locale_{{locale}}.js');
				})

		.constant("settings", {
			ApiUrl : './API/',
			LogURL : './API/Log'
		})

		.config(["$httpProvider",function($httpProvider) {
							
							$httpProvider.interceptors
									.push(function($q, Authorization) {
										return {
											'request' : function(config) {
												var deferred = $q.defer();
												Authorization.authz
														.updateToken(5)
														.success(function() {
																	config.headers = config.headers|| {};
																	config.headers.Authorization = 'Bearer ' + Authorization.authz.token;
																	deferred.resolve(config);
																}).error(
																function() {
																	deferred.reject('Failed to refresh token');
																});
												

												return deferred.promise;
											},
											'responseError' : function(response) {
												if (response.status === 401) {
													console
															.log('session timeout?');
													logout();
												} else if (response.status === 403) {
													alert("Forbidden");
												} else if (response.status === 404) {
													//alert("Not found");
												} else if (response.status) {
													if (response.data
															&& response.data.errorMessage) {
														alert(response.data.errorMessage);
													} else {
														alert("An unexpected server error has occurred");
													}
												}
												return $q.reject(response);
												// return response;
											}
										};
									});
						} ])

		.config([ "$routeProvider", function($routeProvider) {
			$routeProvider.when('/home', {
				templateUrl : 'views/home.html',
				controller : 'cgScreenController',
				resolve : {
					permission : function(Authorization, $route) {
						return Authorization.checkRoutePermission();
					},
				}
			}).when('/screens/:type/:schema/:name*', {
				templateUrl : 'views/cgScreen.html',
				controller : 'cgScreenController',
				resolve : {
					permission : function(Authorization, $route) {
						return Authorization.checkRoutePermission();
					},
				}
			}).when('/NoAccess', {
				templateUrl : 'views/noaccess.html'
			}).otherwise({
				redirectTo : '/home'
			});
		} ])

		.run(function($rootScope) {
			$rootScope.logout = logout;
			$rootScope.unauthorizedRoute = "/NoAccess";
			$rootScope.userSession = auth.authz.tokenParsed;
		});
