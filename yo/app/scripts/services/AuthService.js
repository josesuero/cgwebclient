angular.module('cgWebclientApp').service('Authorization',function($q, $rootScope,$location) {
	'use strict';
	/*
	auth.checkPermission = function(roleCollection) {
		auth.ready().then(function(){
		var passed = false;
		angular.forEach(roleCollection,function(role) {
			if (auth.authz.hasRealmRole(role) || auth.authz.hasResource(role)) {
				passed = true;
			}
		});
		if (passed) {
			$location.path($rootScope.unauthorizedRoute);
			$rootScope.$on('$locationChangeSuccess',function(next,current) {
				deferred.resolve();
			});
		} else {
			deferred.resolve();
		}
		});
	};
	*/
	
	
    auth.checkRoutePermission = function () {
        var roleCollection = ['user'];
    	if ($location.$$path === "/home"){
    		roleCollection = ['user'];
    	}
    	
    	var deferred = $q.defer();
        //var parentPointer = this;

		var passed = false;
		angular.forEach(roleCollection,function(role) {
			if (auth.authz.hasRealmRole(role) || auth.authz.hasResourceRole(role)) {
				passed = true;
			}
		});
		if (!passed) {
			$location.path($rootScope.unauthorizedRoute);
			$rootScope.$on('$locationChangeSuccess',function(next,current) {
				deferred.resolve();
			});
		} else {
			deferred.resolve();
		}
        return deferred.promise;
    };
	
	return auth;
});