/*
angular.module('cgWebclientApp').directive('cgobject', function($compile, cgFactory, $http) {
	// directive factory creates a link function
	return {
		//template: '<div ><p>{{ control.label | translate}}</p><div ng-repeat="item in control.controls"><cgobject id="{{item.name}}" type="{{item.type | lowercase}}" control="item"></cgobject></div></div>'
		//templateUrl:'views/directive-templates/cgobjects/cgopanel.html'
		//template: '<div><p>{{ control.label | translate}}</p>div ng-repeat="item in control.controls"><cgobject id="{{item.name}}" type="{{item.type | lowercase}}" control="item"></cgobject></div></div>'
		scope:{
			control: '=control'
		},
		link : function(scope, element, attrs) {
			scope.$watch(function(scope) {
				return scope.$eval(attrs.id);
			}, function loadForm(name) {
				element.html('<div><p>{{ control.label | translate}}</p>div ng-repeat="item in control.controls"></div></div>');
				$compile(element.contents())(scope);
				//angular.element(".footer").append(element);
			});

		}
	}
});
*/
'use strict';

angular.module('cgWebclientApp')
	.directive('cgobject', function($compile, $templateCache, cleargService, $rootScope) {
		var baseHTML = '<div>' +
			'<label for="{{control.name}}" ng-style="field.lstyle" ng-hide="hidelabel" class="control-label">{{ control.label | translate}}</label>' +
			'</div>';
		return {
			templateUrl: 'views/directive-templates/cgobjects/cgobject.html',
			transclude: true,
			replace: true,
			scope: {
				control: "=control",
				type: "=type",
				dataholder: "=dataholder"
			},
			link: function(scope, element) {
				cleargService.getControl(scope.type).then(function(dataObj) {
					var data = dataObj.data;

					if (data.controller) {
						$rootScope.cgoControllers[scope.type] = new Function('scope', 'cleargService', data.controller);
					}

					var childHTML = baseHTML + data.template;
					element.append($compile(childHTML)(scope));
				}, function notFound() {
					var childHTML = baseHTML + '<span id="{{control.name}}"></span>';
					element.append($compile(childHTML)(scope));
				});
			},
			controller: function($scope) {
				if (!$scope.control.label || $scope.control.label === "") {
					$scope.hidelabel = true;
				} else {
					$scope.hidelabel = false;
				}
				if ($rootScope.cgoControllers[$scope.control.type]) {
					$rootScope.cgoControllers[$scope.control.type]($scope, cleargService);
				}
			}
		};
	});