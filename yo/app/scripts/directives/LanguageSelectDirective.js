angular.module('cgWebclientApp').directive('ngTranslateLanguageSelect', function(LocaleService){
	'use strict';
	return {
		restrict: 'A',
		template: '' + 
		'<div class ="language-select" ng-if"visible">'+
			'<label>'+
				//'{{"directive.language-select.Language" | translate}}: '+
				'<select ng-model="currentLocaleDisplayName"'+
					'ng-options="localesDisplayName for localesDisplayName in localesDisplayNames"'+
					'ng-change="changeLanguage(currentLocaleDisplayName)">'+
				'</select>'+
			'</label>'+
		'</div>'+
		'',
		controller:function($scope){
			$scope.currentLocaleDisplayName = LocaleService.getLocaleDisplayName();
			$scope.localesDisplayNames = LocaleService.getLocalesDisplayNames();
			$scope.visible = $scope.localesDisplayNames && $scope.localesDisplayNames > 1;
			
			$scope.changeLanguage = function(locale){
				LocaleService.setLocaleByDisplayName(locale);
				
			};
		}
	};
	
});