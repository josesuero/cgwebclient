angular.module('cgWebclientApp').controller("cgScreenController", ['$scope', '$route', '$routeParams', '$q', 'Authorization', "cleargService", "$ocLazyLoad", "$rootScope", function($scope, $route, $routeParams, $q, Authorization, cleargService, $ocLazyLoad, $rootScope) {
	"use strict";
	var control = {};
	control.name = $routeParams.schema + "." + $routeParams.name;
	control.label = "Name";
	control.type = "CGOFORM";
	control.screenname = "CGAPP.CGAP_CGDBFORM";
	control.screendata = {
		"name": "CGERP.AR_INVOICES"
	}
	$scope.control = control;
	//$ocLazyLoad.load('testModule.js');
	// $rootScope.cgoControllers["CGOFORM"] = function(scope) {
	// 	cleargService.exec(scope.control.screenname, scope.control.screendata).then(function(dataObj) {
	// 		var data = dataObj.data;
	// 		var lookup = {};
	// 		var controls = [];
	// 		data.controls.forEach(function(item) {
	// 			item.controls = [];
	// 			lookup[item.name] = item;
	// 		});

	// 		for (var key in lookup) {
	// 			var item = lookup[key];
	// 			var parent = lookup[item.container];
	// 			if (parent) {
	// 				//item.parent = parent;
	// 				parent.controls.push(item);
	// 			} else {
	// 				controls.push(item);
	// 			}
	// 		}

	// 		data.controls = controls;
	// 		scope.screen = data;
	// 		scope.screen.name = name;
	// 		scope.childDataholder = {};
	// 		scope.dataholder[scope.control.name] = scope.childDataholder;
	// 	});

	// }
	$scope.dataholder = {};
}]);