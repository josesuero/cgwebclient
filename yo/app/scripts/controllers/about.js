'use strict';

/**
 * @ngdoc function
 * @name cgWebclientApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the cgWebclientApp
 */
angular.module('cgWebclientApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
