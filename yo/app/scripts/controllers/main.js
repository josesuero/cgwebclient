'use strict';

/**
 * @ngdoc function
 * @name cgWebclientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the cgWebclientApp
 */
angular.module('cgWebclientApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
