import javax.annotation.Resource;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.infinispan.manager.CacheContainer;

@RequestScoped
@Path("/test")
public class Test {
	@Resource(lookup="java:infinispan/clearg")
	private CacheContainer container;
	
	private org.infinispan.Cache<String, String> cache;
	
	public Test(){
		cache = container.getCache();
	}
	
	@GET
	public String test(){
		return "";
		
	}

}
