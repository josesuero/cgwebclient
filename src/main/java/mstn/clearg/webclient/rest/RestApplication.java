package mstn.clearg.webclient.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/clearg")
public class RestApplication extends Application {

}
