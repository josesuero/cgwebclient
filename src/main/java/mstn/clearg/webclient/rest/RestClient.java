package mstn.clearg.webclient.rest;

import javax.ws.rs.core.Form;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonException;


import java.util.Iterator;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.jboss.resteasy.util.Encode;

@SuppressWarnings("rawtypes")
public class RestClient {

	// CONSTRUCTOR
	public RestClient() {

	}
/**
 * Function para hacer requerimientos a WebService REST
 * @param client
 * @param requestObj JSONObject opciones:
 * url - String
 * headers - JSONArray {name,value}
 * params - JSONArray {name,value}
 * accept - String
 * type - String
 * entity - String
 * cookies - JSONArray {name,value}
 * method - String  
 * @return
 * @throws JSONException
 * 
 * 
 */

	public static JsonObject request(ResteasyClient client, String url, JsonObject requestObj) throws JsonException {
		ResteasyWebTarget target = client.target(url);
		
		
		String type = "text/plain";
		
		if (requestObj.containsKey("type")){
			type = requestObj.getString("type");
		}			
		
		
		String method = "get";
		
		if (requestObj.containsKey("method")){
			method = requestObj.getString("method");
		}
		
        
        Response response;
        if (method.equals("post")){

        	Form form = new Form();
    		if (requestObj.containsKey("params")) {
    			
    			JsonObject params = requestObj.getJsonObject("params");
    			for (Iterator iter = params.keySet().iterator(); iter.hasNext(); ) {
    				String key = iter.next().toString();
    				form.param(key,params.getString(key));
    			}
    		}
    		
    		Entity<Form> entity = Entity.form(form);
            
    		Builder build= target.request(type);
    		response = build.post(entity);
        } else {
    		if (requestObj.containsKey("params")) {
    			JsonObject params = requestObj.getJsonObject("params");
    			for (Iterator iter = params.keySet().iterator(); iter.hasNext(); ) {
    				String key = iter.next().toString();
    				target = target.queryParam(key,Encode.encodeQueryParamAsIs(params.getString(key)));
    			}
    		}
    		Builder build= target.request(type);
        	response = build.get();
        }
		String value = response.readEntity(String.class);
		JsonObject output = Json.createObjectBuilder()
		.add("content", value)
		.add("status", response.getStatus())
		.build();
		response.close();
		//client.close();
		return output;
/*			//Client client = ClientBuilder.newClient();
			String url = requestObj.getString("url");
			// , String method, JSONArray headers, acceptMediaType
			
			WebTarget webResource = client.target(url);
			
			String type = "text/plain";
			String entitytype= "Text/Plain";
			
			if (requestObj.containsKey("type")){
				type = requestObj.getString("type");
				entitytype = type;
			}			
			
			//WebResource webResource = client.resource(url);
			
			if (requestObj.containsKey("params")) {
				JsonObject params = requestObj.getJsonObject("params");
				for (Iterator iter = params.keySet().iterator(); iter.hasNext(); ) {
					String key = iter.next().toString();
					webResource = webResource.queryParam(key, Encode.encodeQueryParamAsIs(params.getString(key)));
				}
			}
			
			Builder builder = webResource.request(type);
			
			// .resource("http://localhost:8080/JerseyWebService/rest/WebService");
			if (requestObj.containsKey("headers")) {
				JsonArray headers = requestObj.getJsonArray("headers");
				for (int i = 0; i < headers.size(); i++) {
					JsonObject header = headers.getJsonObject(i);
					builder = builder.header(header.getString("name"),
							header.getString("value"));
				}
			}


						
			if (requestObj.containsKey("accept")){
				builder = builder.accept(requestObj.getString("accept"));	
			}
			
			
			
			if (requestObj.containsKey("entity")){
				entitytype = requestObj.getString("entity");
			}

			if (requestObj.containsKey("cookies")){
				JsonObject cookies = requestObj.getJsonObject("cookies");
				for (Iterator iter = cookies.keySet().iterator(); iter.hasNext(); ) {
					String key = iter.next().toString();
					builder = builder.cookie(new Cookie(key, cookies.getString(key)));
				}
			}
			
			String method = "get";
			if (requestObj.containsKey("method")){
				method = requestObj.getString("method").toLowerCase();
			}
			
			Response response;
			if (method.equals("put")){
				response = builder.put(Entity.entity(entitytype,type));
			} else if (method.equals("post")){
				response = builder.post(Entity.entity(entitytype,type));
			} else if (method.equals("delete")){
				response = builder.delete();
			} else {
				response = builder.get();
			}
			JsonObject output = Json.createObjectBuilder()
			.add("content", response.readEntity(String.class))
			.add("status", response.getStatus())
			.build();
			response.close();
			//client.close();
			return output;*/
	}

}