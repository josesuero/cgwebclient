package mstn.clearg.webclient.webservices;

import javax.enterprise.context.RequestScoped;

import java.io.StringReader;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import mstn.clearg.webclient.rest.RestClient;

import javax.json.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/*
import mstn.clearg.appengine.CGAppEngine;
import mstn.clearg.cache.CGCache;
import mstn.clearg.cache.CGCache.CacheLevel;

*/


@RequestScoped
@Path("/server")
public class Server {
	
	@Context HttpServletRequest request;
	//private CGCache cacheGlobal;
	ResteasyClient client;
	
	public Server(){
		//cacheGlobal = new CGCache("", "", CacheLevel.Global);
		client = new ResteasyClientBuilder().build();
	}
	
	@PostConstruct
	public void init(){
		
	}
	
	private static String getGlobalVar(String name) throws NamingException {
		return (new InitialContext()).lookup(name).toString();
	}
	
	public JsonObject process(String procedure, String data) throws Exception{
		String schema = "NONE";
		Pattern pattern = Pattern.compile("^(.*?)\\..*");
		Matcher matcher = pattern.matcher(request.getServerName());
		if (matcher.matches())
			schema = matcher.group(1);
		
		JsonObjectBuilder requestObj = Json.createObjectBuilder();
		String url = new StringBuilder(getGlobalVar("java:global/cgApp-Url")).append("server").toString();
		JsonObject params = Json.createObjectBuilder()
				.add("procedure",procedure)
				.add("data", data)
				.add("schema", schema)
				.add("token", "xxx")
				.build();
		requestObj.add("params", params);
		requestObj.add("method", "post");
		requestObj.add("type", MediaType.APPLICATION_JSON);
		
		JsonObject result = RestClient.request(client, url, requestObj.build());
		
		JsonReader jsonReader = Json.createReader(new StringReader(result.getString("content")));
		JsonObject object = jsonReader.readObject();
		jsonReader.close();
		
		return object;
		
		/*
		 * String schema = CGAppEngine.getSchema(request.getServerName());
		if (schema == null){
			schema = "none";
		}
		String token = (String) session.getAttribute("token");
		if (token == null){
			token = (String) session.getAttribute("innerToken");
		}
		CGAppEngine cgAppEngine = new CGAppEngine(schema, token);
		
		return cgAppEngine.senddata(procedure.toUpperCase(), data);
		*/
	}
	
	@GET
	public String getRegular(@QueryParam("procedure") String procedure, @QueryParam("data") String data) throws Exception{
		return process(procedure, data).toString();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String postRegular(@FormParam("procedure") String procedure, @FormParam("data") String data) throws Exception{
		return process(procedure, data).toString();
	}

	@GET
	@Path("/cache/{type:.+}/{procedure:.*}.cg")
	public Response getcachejson(@PathParam("procedure") String procedure, @QueryParam("data") String data, @PathParam("type") String type, @QueryParam("cached") String cache) throws Exception{
		String content = null;
		//boolean cached;
		//String status = "200";
		//String cacheName = CGAppEngine.concat("GLOBAL_PROCEDURES_", procedure,data);
		//if (cache != null && cache.equalsIgnoreCase("false")){
		//	cached = false;
		//} else {
		//	cached = true;
		//}
		
		//if (cacheGlobal != null && cached == true) {
		//	content = (String) cacheGlobal.get(cacheName);
		//}
		if (content == null){
			JsonObject procedureResult = process(procedure, data); 
			content = procedureResult.getString("content");
			//status = procedureResult.getString("status");
		}
		//if (cached == true && status.equals("200")){
		//	cacheGlobal.put(cacheName, content);	
		//} else {
		//	if (cacheGlobal.exists(cacheName)){
		//		cacheGlobal.remove(cacheName);
		//	}
		//}
    	String mediaType = "aplication/json";
    	if (type.equalsIgnoreCase("json")){
    		mediaType = MediaType.APPLICATION_JSON;
    	} else if (type.equalsIgnoreCase("html")) {
    		mediaType = MediaType.TEXT_HTML;
    	} else if (type.equalsIgnoreCase("text")){
    		mediaType = MediaType.TEXT_PLAIN;
    	} else if (type.equalsIgnoreCase("xml")){
    		mediaType = MediaType.APPLICATION_XML;
    	} else if (type.equalsIgnoreCase("js")){
    		mediaType = "text/javascript";
    	}
    	return Response.ok(content).type(mediaType).build();
		
	}
	
	@GET
	@Path("/{type:.+}/{procedure:.*}.cg")
	public Response getjson(@PathParam("procedure") String procedure, @QueryParam("data") String data, @PathParam("type") String type) throws Exception{
		
		if (data == null){
			if (request.getQueryString() != null){
				JsonObjectBuilder dataObj = Json.createObjectBuilder();
				Enumeration<String> params = request.getParameterNames(); 
				while (params.hasMoreElements()){
					String paramName = params.nextElement();
					dataObj.add(paramName, request.getParameter(paramName));
				}
				data = dataObj.build().toString();
			}
		}
		
    	String content = process(procedure, data).get("content").toString();
    	String mediaType = "aplication/json";
    	if (type.equalsIgnoreCase("json")){
    		mediaType = MediaType.APPLICATION_JSON;
    	} else if (type.equalsIgnoreCase("html")) {
    		mediaType = MediaType.TEXT_HTML;
    	} else if (type.equalsIgnoreCase("text")){
    		mediaType = MediaType.TEXT_PLAIN;
    	} else if (type.equalsIgnoreCase("xml")){
    		mediaType = MediaType.APPLICATION_XML;
    	}
    	return Response.ok(content).type(mediaType).build();

	}

	
	@GET
	@Path("/{procedure:.*}.cg")
	public String get(@PathParam("procedure") String procedure, @QueryParam("data") String data) throws Exception{
		return process(procedure, data).toString();
	}
	
	
	@POST
	@Path("/{type:.+}/{procedure:.*}.cg")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response postType(@PathParam("procedure") String procedure, @FormParam("data") String data, @PathParam("type") String type) throws Exception{
		return getjson(procedure, data, type);
	}
	
	@POST
	@Path("/{procedure:.*}.cg")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String post(@PathParam("procedure") String procedure, @FormParam("data") String data) throws Exception{
		return process(procedure, data).toString();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_JSON)
	public String postjson(@FormParam("procedure") String procedure, @FormParam("data") String data) throws Exception{
		return process(procedure, data).getString("content");
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.TEXT_PLAIN)
	public String posttext(@FormParam("procedure") String procedure, @FormParam("data") String data) throws Exception{
		return process(procedure, data).getString("content");
	}

}
